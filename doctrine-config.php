<?php 

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

/**
 * Generar el gestor de entidades
 * 
 */
function getEntityManager()
{
    $dbParams = array(
        'host' => CONFIGS['database']['server'],
        'port' => CONFIGS['database']['port'],
        'dbname' => CONFIGS['database']['name'],
        'user' => CONFIGS['database']['user'],
        'password' => CONFIGS['database']['pass'],
        'driver' => CONFIGS['database']['driver']
    );

    $config = Setup::createAnnotationMetadataConfiguration(
        array('./App/Database/Models/'),
        CONFIGS['database']['debug'],
        ini_get('sys_temp_dir'),
        null, 
        false
    );
    $config->setAutoGenerateProxyClasses(true);

    return EntityManager::create($dbParams, $config);
}

// DATABASE_URL="mysql://root:rootsymfony-api-platform-db:3306/symfony_db?serverVersion=8.0"