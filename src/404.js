import React from 'react'
import ReactDOM from 'react-dom'
import NotFound from './Components/404/404'

ReactDOM.render(<NotFound />, document.getElementById('404'));