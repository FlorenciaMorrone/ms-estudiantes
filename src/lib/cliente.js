import { ControlCamera } from '@material-ui/icons';
import axios from 'axios'

/**
 * @author DamianDev <damian.gonzalez@hamiltonandlovelace.com>
 * 
 * Cliente de peticiones HTTP con axios
 * 
 * Tiene la responsabilidad de crear todas las peticiones HTTP al servidor de acuerdo al 
 * estandar de creacion de peticiones H&L
 */

class Cliente{
    
    constructor(endpoint = "", metodo = "", data = {}, filtros = {}, archivos={}){
        this.endpoint = endpoint;
        this.metodo = metodo;
        this.data = data;
        this.filtros = filtros;
        this.archivos = archivos;
        this.formData = new FormData();
        this.makeForm();
    }

    execute(){
        const peticion = axios({
            method: "POST",
            url: this.endpoint,
            data: this.formData
        })
        return peticion;
    }

    makeForm(){
        this.formData.append("_method", this.metodo);
        this.formData.append("_data", JSON.stringify(this.data));
        this.formData.append("_filters", JSON.stringify(this.filtros));
    }
}

export default Cliente;