import React from 'react'
import './404.scss'

export default function Notfound()
{
    return (
        <div className={'Error'}>
            <div className={'error-imagen'}>
                <figure>
                    <img src="public-assets/imagenes?path=404.png" alt="404"/>
                </figure>
            </div>
            <div className={'mensaje'}>
                <h3><strong>¡Cielos!</strong> Parece que no existe la página que estas buscando</h3>
            </div>
        </div>
    )
}