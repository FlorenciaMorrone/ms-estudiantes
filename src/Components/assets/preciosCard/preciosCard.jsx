import React from 'react'
import './preciosCard.scss'

export default function PreciosCard({titulo, precio, descripcion, elementos, ancho})
{
    return (
        <div className={'PreciosCard'} style={{width: ancho}}>
            <div className={'PreciosCard-titulo'}>
                <h2>{titulo}</h2>
            </div>
            <hr/>
            <div className={'PreciosCard-elementos'}>
                <strong>{elementos}</strong>
            </div>
            <hr/>
            <div className={'PreciosCard-descripcion'}>
                <p>{descripcion}</p>
            </div>
            <hr/>
            <div className={'PreciosCard-precio'}>
                <strong>{precio}</strong>
                <small>Ya incluye IVA</small>
                <small>Costo anual</small>
            </div>
        </div>
    )
}