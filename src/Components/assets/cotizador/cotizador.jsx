import React, { useState, useCallback } from 'react'
import './cotizador.scss'
import funciones from '../../../lib/funciones'
import Cliente from '../../../lib/cliente'
import Recaptcha from 'react-recaptcha'

export default function Cotizador()
{
    const [cliente, setCliente] = useState({})
    const [token, setToken] = useState('')
    const [datos, setDatos] = useState({
        correo: ''
    })
    const [errorCorreo, setErrorCorreo] = useState('')
    const [errorToken, setErrorToken] = useState('')
    const alCambiarInput = useCallback((e) => {
        setErrorCorreo('')
        setDatos({
            ...datos,
            [e.target.name]: e.target.value
        })
    })
    const buscarCliente = useCallback(() => {
        if(datos.correo === ''){
            setErrorCorreo('Debes ingresar tu correo')
        }
        else if(token === ''){
            setErrorToken('Debes validar el captcha')
        }
        else{
            const request = new Cliente('clientes', 'buscar-por-correo', {
                correo: datos.correo,
                captcha: token
            })
            request.execute().then(response => {
                if(response.data._errors){
                    if(response.data._errors.params.correo.descripcion)
                        setErrorCorreo(response.data._errors.params.correo.descripcion)
                    if(response.data._errors.params.captcha.descripcion)
                        setErrorToken(response.data._errors.params.captcha.descripcion)
                }
                else if(response.data._data.mensaje === "Cliente no registrado")
                    setCliente({
                        ...cliente,
                        primer_nombre: '',
                        segundo_nombre: '',
                        primer_apellido: '',
                        segundo_apellido: '',
                        telefono: '',
                        correo: datos.correo
                    })
                else
                    setCliente({
                        ...cliente,
                        primer_nombre: response.data._data.cliente.primer_nombre,
                        segundo_nombre: response.data._data.cliente.segundo_nombre,
                        primer_apellido: response.data._data.cliente.primer_apellido,
                        segundo_apellido: response.data._data.cliente.segundo_apellido,
                        telefono: response.data._data.cliente.telefono,
                        correo: response.data._data.cliente.correo
                    })
            })
        }
        
    }, [datos, errorCorreo, cliente, token]);

    const cotizar = useCallback(() => {
        console.log(token)
    }, [token])

    const refrescar = useCallback(() => {
        setCliente({})
        setDatos({})
        setErrorToken('')
        setToken('')
    })

    const onCaptchaVerify  = (e) => {
        setToken(e)
    }
    
    const callback = () => {
        console.log('Hecho :)')
    }

    return(
        <div className={'Cotizador'}>
            {(funciones.isObjEmpty(cliente)) ? (
                <div className={'buscador-clientes'}>
                    <label htmlFor="correo">¿Ya has cotizado antes?</label>
                    <input type="mail" name="correo" placeholder="Ingresa tu correo" onChange={alCambiarInput}/>
                    {(errorCorreo !== '') ? (
                        <span>{errorCorreo}</span>
                    ) : ('')}
                    <Recaptcha
                        sitekey="6LcGCNQaAAAAAKQ40kYiiuoFUOBERDma84OyJPOW"
                        render="explicit"
                        verifyCallback={onCaptchaVerify}
                        onloadCallback={callback}
                    />
                    {(errorToken !== '') ? (
                        <span>{errorToken}</span>
                    ) : ('')}
                    <button onClick={buscarCliente}>Buscar</button>
                </div>
            ) : (
                <div className={'formulario-cotizador'}>
                    <div className={'formulario-cotizador-contacto'}>
                        <h3>Datos de contacto</h3>
                        <div>
                            <label htmlFor="primer_nombre">Primer nombre</label>
                            <input type="text" name="primer_nombre" value={cliente.primer_nombre}/>
                        </div>
                        <div>
                            <label htmlFor="segundo_nombre">Segundo nombre</label>
                            <input type="text" name="segundo_nombre" value={cliente.segundo_nombre}/>
                        </div>
                        <div>
                            <label htmlFor="primer_apellido">Primer apellido</label>
                            <input type="text" name="segundo_apellido" value={cliente.primer_apellido}/>
                        </div>
                        <div>
                            <label htmlFor="segundo_apellido">Segundo</label>
                            <input type="text" name="segundo_apellido" value={cliente.segundo_apellido}/>
                        </div>
                        <div>
                            <label htmlFor="telefono">Telefono</label>
                            <input type="text" name="segundo_apellido" value={cliente.telefono}/>
                        </div>
                        <div>
                            <label htmlFor="correo">Correo</label>
                            <input type="text" name="correo" value={cliente.correo}/>
                        </div>
                    </div>
                    <div className={'formulario-cotizador-servicios'}>
                        <h3>Cuentanos que necesitas</h3>
                        <div>
                            <label htmlFor="nombre_proyecto">¿Cómo se llama tu proyecto?</label>
                            <input type="text" name="nombre_proyecto"/>
                        </div>
                        <div>
                            <label htmlFor="nombre_proyecto">¿Cuanto tiempo tienes para desarrollarlo?</label>
                            <input type="text" name="nombre_proyecto"/>
                        </div>
                        <div>
                            <label htmlFor="descripcion_proyecto">Describenos tu proyecto</label>
                            <textarea name="descripcion_proyecto" rows="10"></textarea>
                        </div>
                        <div>
                            <Recaptcha
                                sitekey="6LcGCNQaAAAAAKQ40kYiiuoFUOBERDma84OyJPOW"
                                render="explicit"
                                verifyCallback={onCaptchaVerify}
                                onloadCallback={callback}
                            />
                        </div>
                        <div>
                            <button onClick={refrescar}>Refrescar</button>
                            <hr/>
                            <button onClick={cotizar}>Cotizar</button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}