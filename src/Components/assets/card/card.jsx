import React from 'react'
import './card.scss'

export default function Card({titulo = null, descripcion = '', imagen = null, altImagen = null, precio = null, ancho ='20%'})
{
    return (
        <div className={'Card'} style={{width: ancho}}>
                {(imagen) ? (
                <figure className={'imagen'}>
                    <img src={imagen} alt={altImagen} loading="lazy"/>
                </figure>
                ) : 
                ('')}
            <section className={'descripcion'}>
                <h3>{titulo}</h3>
                <hr/>
                <p>{descripcion}</p>
                <hr/>
                {(precio) ? (
                    <strong>{precio}</strong>
                ) : ('')}
            </section>
        </div>
    )
}