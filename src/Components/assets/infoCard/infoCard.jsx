import React from 'react'
import './infoCard.scss'

export default function InfoCard({imagen, titulo, altImagen, descripcion, disposicion})
{
    return (
        <div className={'InfoCard'}>
            {(disposicion == 'derecha') ? (
                <div className={'carta'}>
                    <div className={'descripcion'}>
                        <h2>{titulo}</h2>
                        <hr/>
                        <p>{descripcion}</p>
                    </div>
                    <div className={'imagen'}>
                        <figure>
                            <img src={imagen} alt={altImagen} loading="lazy"/>
                        </figure>
                    </div>
                </div>
            ) : (
                <div className={'carta'}>
                    <div className={'imagen'}>
                        <figure>
                            <img src={imagen} alt={altImagen} loading="lazy"/>
                        </figure>
                    </div>
                    <div className={'descripcion'}>
                        <h2>{titulo}</h2>
                        <hr/>
                        <p>{descripcion}</p>
                    </div>
                </div>
            )}
        </div>
    )
}