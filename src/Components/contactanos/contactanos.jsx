import React from 'react'
import Nav from '../general/nav'
import Footer from '../general/footer'
import './contactanos.scss'

export default function Contactanos()
{
    return (
        <div className={'Contactanos'}>
             <Nav />
             <div>
                 Contactanos
             </div>
             <Footer />
        </div>
    )
}