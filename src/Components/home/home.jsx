import React from 'react'
import './home.scss'
import Nav from '../general/nav'
import Footer from '../general/footer'
import InfoCard from '../assets/infoCard/infoCard'
import Card from '../assets/card/card'

export default function Home()
{
    return (
        <div className={'Home'}>
            <Nav />
            <section className={'header'}>
                <div className={'figura'}>
                    <figure>
                        <img src="public-assets/imagenes?path=logo_mascota.png" alt="Logo de la empresa"/>
                    </figure>
                </div>
                <div className={'slider'}>
                    <h1>"Cumplimos tus metas mientras crecemos juntos"</h1>
                </div>
            </section>
            <section className={'contenido'}>
                <InfoCard 
                    imagen="public-assets/imagenes?path=transformacion.png"
                    titulo="Transformacion digital"
                    altImagen="Transformacion digital"
                    descripcion="Transformamos a nuestros clientes utilizando tecnología innovadora, económica, eficiente y efectiva para crear plataformas personalizadas que mejoran la experiencia de usuario, agilizan procesos y ayudan a optimizar el flujo interno del negocio."
                    disposicion="derecha"
                />
                <InfoCard 
                    imagen="public-assets/imagenes?path=construccion.png"
                    titulo="Tecnología de primera generación"
                    altImagen="Tecnologia inovadora"
                    descripcion="Nuestro objetivo es poner a tu alcance la mejor tecnología para automatizar procesos y controlar tu información trabajando en conjunto para lograr ser tu mejor opción."
                    disposicion="izquierda"
                />
                <InfoCard 
                    imagen="public-assets/imagenes?path=optimizamos.png"
                    titulo="Optimizamos procesos"
                    altImagen="Optimizamos procesos"
                    descripcion="Entendemos lo importante que es para nuestros clientes disminuir los procesos repetitivos y por ello ponemos a tu alcance las herramientas necesarias para que solo te dediques a lo mas importante, atender tu negocio."
                    disposicion="derecha"
                />
                <InfoCard 
                    imagen="public-assets/imagenes?path=escalabilidad.png"
                    titulo="Creamos herramientas escalables"
                    altImagen="Escalabilidad"
                    descripcion="Nuestra tecnología esta diseñada para que escale al mismo ritmo que tu negocio, sin restricciones ni cuotas por usuarios, sin grandes inversiones, lo que necesitas cuando lo necesitas."
                    disposicion="izquierda"
                />
                <InfoCard 
                    imagen="public-assets/imagenes?path=costo.png"
                    titulo="La mejor relacion costo-beneficio"
                    altImagen="Relacion costo-beneficio"
                    descripcion="No importa si tu negocio es pequeño, mediano o grande, nuestra tecnologia se adapta a tus necesidades con el mejor precio, ofreciendote las mejores funcionalidades."
                    disposicion="derecha"
                />
            </section>
            <section className={'servicios'}>
                <h2>¿Qué podemos hacer?</h2>
                <hr/>
                <div className={'lista-servicios'}>
                    <Card 
                        titulo="Tiendas digitales"
                        descripcion="Gestiona tu tienda digital, crea puntos de venta, administra tus productos y haz tan grande tu tienda como tu negocio lo requiera, sin limites de usuarios ni productos, olvidate del pago por consumo y altas comisiones."
                        imagen="public-assets/imagenes?path=tienda.png"
                        altImagen="Tiendas digitales"
                    />
                    <Card 
                        titulo="Agendas digitales"
                        descripcion="Gestiona reuniones con tus clientes, proveedores, empleados, etc. sin limites de usuarios y sin limites de consumo"
                        imagen="public-assets/imagenes?path=agenda.png"
                        altImagen="Agendas digitales"
                    />
                    <Card 
                        titulo="Sistemas ERP"
                        descripcion="Administra, gestiona y optimiza. Todo en un solo lugar, crea un sistema robusto sin complicaciones, escala y selecciona solo lo que necesitas. Olvidate de cuotas mensuales forzosas."
                        imagen="public-assets/imagenes?path=erp.png"
                        altImagen="Tiendas digitales"
                    />
                    <Card 
                        titulo="Eventos digitales"
                        descripcion="Organiza eventos públicos o privados, visualiza tu lista de participantes, asigna ponentes, crea eventos virtuales y visualizalos en tu sistema ERP personalizado."
                        imagen="public-assets/imagenes?path=eventos.png"
                        altImagen="Tiendas digitales"
                    />
                </div>
            </section>
            <section className={'micro-servicios'}>
                <h2>¿Como lo hacemos?</h2>
                <hr/>
                <div>
                <InfoCard 
                    imagen="public-assets/imagenes?path=micros.png"
                    titulo="Los Micro Servicios H&L"
                    altImagen="Micro Servicios"
                    descripcion="Son una pequeña pieza de software especialmente diseñada para atender una responsabilidad puntual y finita. Estos tienen la peculiaridad de que son construidos con capacidades limitadas de cómputo, haciéndolos extraordinariamente flexibles y adaptables a las diferentes necesidades de los clientes. Se pueden desempeñar de forma independiente y descentralizada. Además, al ser tan pequeños y puntuales nos proveen una gran capacidad de escalarlos a modo que podamos atender la demanda de nuestras aplicaciones de una forma sencilla, económica y transparente."
                    disposicion="izquierda"
                />
                <InfoCard 
                    imagen="public-assets/imagenes?path=flexibilidad.png"
                    titulo="Flexibilidad total"
                    altImagen="Flexibilidad en el desarrollo"
                    descripcion="Los Micro Servicios H&L proponen una forma diferente de administrar nuestros recursos informáticos, inyectan el poder de cómputo necesario a las diversas necesidades de nuestros clientes, de una forma personalizada, escalable y robusta, además proponen un canal de expansión sencillo y transparente."
                    disposicion="izquierda"
                />
                <InfoCard 
                    imagen="public-assets/imagenes?path=escala.png"
                    titulo="Escalabilidad personalizada"
                    altImagen="Escalabilidad"
                    descripcion="Entendemos la importante necesidad de nuestros clientes en incrementar su flujo de ventas, estandarizar sus procesos internos y sobre todo mantener el control sobre su información. Es por ello que los Micro Servicios H&L están diseñados para que las aplicaciones de nuestros clientes crezcan al ritmo que ellos lo necesitan, siguiendo siempre la filosofía de ni más ni menos, solo lo necesario."
                    disposicion="izquierda"
                />
                </div>
            </section>
            <section className={'nosotros'}>

            </section>
            <Footer />
        </div>
    )
}