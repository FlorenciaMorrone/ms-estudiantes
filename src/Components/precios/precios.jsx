import React from 'react'
import Nav from '../general/nav'
import Footer from '../general/footer'
import PreciosCard from '../assets/preciosCard/preciosCard'
import Cotizador from '../assets/cotizador/cotizador'
import './precios.scss'

export default function Precios()
{
    return (
        <div className={'Precios'}>
            <Nav />
            <section className={'contenido'}>
                <h2>Conoce nuestra oferta</h2>
                <hr/>
                <article className={'tabla-precios'}>
                    <PreciosCard 
                        titulo="Promo 1"
                        precio="$12,480.15 MXN"
                        descripcion="Ideal para pequeños emprendedores, crea un canal de comunicacion directa con tus clientes y gestionalo todo de forma segura."
                        elementos="Tienda digital + Panel Admin + Plataforma web pública"
                        ancho="30%"
                    />
                    <PreciosCard 
                        titulo="Promo 2"
                        precio="$16,640.20 MXN"
                        descripcion="Ideal para pequeños y medianos negocios, crea canales de comunicación y mantén informados a tus clientes con un blog a tu medida."
                        elementos="Tienda digital + Panel Admin + Plataforma web pública + Blog"
                        ancho="30%"
                    />
                    <PreciosCard 
                        titulo="Promo 3"
                        precio="$20,800.25 MXN"
                        descripcion="Da el salto, ideal para negocios en crecimiento. Gestiona, informa y agenda citas con tus clientes. Todo en un solo lugar y sin complicaciones."
                        elementos="Tienda digital + Panel Admin + Plataforma web pública + Blog + Agenda digital"
                        ancho="30%"
                    />
                </article>
                <hr/>
                <article className={'tabla-capacidades'}>
                    <h2>¿Necesitas un desarrollo a medida?</h2>
                    <hr/>
                    <Cotizador />
                </article>
            </section>
            <Footer />
        </div>
    )
}