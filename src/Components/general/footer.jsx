import React from 'react'
import './footer.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone, faMailBulk } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faInstagram, faLinkedin, faTwitter } from '@fortawesome/free-brands-svg-icons'

export default function Footer()
{
    return(
        <footer className={'Footer'}>
            <div className={'informacion-empresa'}>
                <div className={'datos-empresa'}>
                    <strong>Hamilton And Lovelace S.A.S</strong><br/>
                    <small>Cumplimos tus metas mientras crecemos juntos</small>
                    <hr/>
                    <p>Huixquilucan Estado de Mexico</p>
                    <ul>
                        <li>
                            <FontAwesomeIcon icon={faPhone} /> 5610677185
                        </li>
                        <li>
                            <FontAwesomeIcon icon={faPhone} /> 5585133186
                        </li>
                        <li>
                            <FontAwesomeIcon icon={faMailBulk} /> info@hamiltonandlovelace.com
                        </li>
                    </ul>
                </div>  
                <div className={'redes-empresa'}>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/HamiltonAndLovelace" target="_blank" rel="noopener" aria-label="Facebook">
                                <FontAwesomeIcon icon={faFacebook} />
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/hamiltonandlovelace/" target="_blank" rel="noopener" aria-label="Instagram">
                                    <FontAwesomeIcon icon={faInstagram} />
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/hamilton-lovelace/?originalSubdomain=mx" target="_blank" rel="noopener" aria-label="Linkedin">
                                <FontAwesomeIcon icon={faLinkedin} />
                            </a>
                        </li>
                        <li>
                            <a href="https://mobile.twitter.com/hamiltonandlove" target="_blank" rel="noopener" aria-label="Twitter">
                                <FontAwesomeIcon icon={faTwitter} />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div className={'derechos-autor'}>
                <div>
                    <p>Propiedad de Hamilton And Lovelace S.A.S</p>
                    <ul>
                        <li>
                            <a href="/privacidad" target="_blank" rel="noopener" aria-label="Privacidad">
                                Aviso de privacidad
                            </a>
                        </li>
                        <li>
                            <a href="https://storyset.com" target="_blank" rel="noopener" aria-label="Freepik">
                                Illustration by Freepik Storyset
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    )
}