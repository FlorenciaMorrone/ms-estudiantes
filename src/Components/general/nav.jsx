import React from 'react'
import './nav.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faDollarSign, faMailBulk } from '@fortawesome/free-solid-svg-icons'

export default function Nav()
{
    return (
        <nav className={'Nav'}>
            <ul>
                <li>
                    <a href="/" aria-label="Home">
                        <FontAwesomeIcon icon={faHome} /> <strong>Home</strong>
                    </a>
                </li>
                <li>
                    <a href="/precios" aria-label="Precios">
                        <FontAwesomeIcon icon={faDollarSign} /> <strong>Precios</strong>
                    </a>
                </li>
                <li>
                    <a href="/contactanos" aria-label="Contactanos">
                        <FontAwesomeIcon icon={faMailBulk} /> <strong>Contáctanos</strong>
                    </a>
                </li>
            </ul>
        </nav>
    )
}