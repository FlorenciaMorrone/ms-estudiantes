<?php 

use Doctrine\ORM\Tools\Console\ConsoleRunner;

$configs = json_decode(@file_get_contents('./config.json'), true);

define("CONFIGS", $configs);

require_once __DIR__.'/doctrine-config.php';

$entityManager = getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);