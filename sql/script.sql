CREATE SCHEMA IF NOT EXISTS `estudiantes` DEFAULT CHARACTER SET utf8 ;
USE `estudiantes` ;

-- -----------------------------------------------------
-- Table `estudiantes`.`sesion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estudiantes`.`sesion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `createdAt` DATETIME NOT NULL,
  `updatedAt` DATETIME NULL,
  `hash_publico` VARCHAR(255) NOT NULL,
  `hash_privado` VARCHAR(255) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `usuario_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sesion_usuario1_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_sesion_usuario1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `estudiantes`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estudiantes`.`direccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estudiantes`.`direccion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `pais` VARCHAR(45) NOT NULL,
  `provincia` VARCHAR(80) NOT NULL,
  `ciudad` VARCHAR(80) NOT NULL,
  `codigo_postal` VARCHAR(8) NOT NULL,
  `calle` VARCHAR(45) NOT NULL,
  `numero_principal` VARCHAR(5) NOT NULL,
  `numero_secundario` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estudiantes`.`escuela`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estudiantes`.`escuela` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `grado` VARCHAR(2) NOT NULL,
  `grupo` VARCHAR(2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estudiantes`.`estudiante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estudiantes`.`estudiante` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `primer_nombre` VARCHAR(45) NOT NULL,
  `segundo_nombre` VARCHAR(45) NULL,
  `primer_apellido` VARCHAR(45) NOT NULL,
  `segundo_apellido` VARCHAR(45) NULL,
  `sexo` VARCHAR(15) NOT NULL,
  `telefono` VARCHAR(45) NOT NULL,
  `nombre_contacto` VARCHAR(45) NOT NULL,
  `telefono_contacto` VARCHAR(45) NOT NULL,
  `direccion_id` INT NOT NULL,
  `escuela_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_estudiante_direccion1_idx` (`direccion_id` ASC),
  INDEX `fk_estudiante_escuela1_idx` (`escuela_id` ASC),
  CONSTRAINT `fk_estudiante_direccion1`
    FOREIGN KEY (`direccion_id`)
    REFERENCES `estudiantes`.`direccion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_estudiante_escuela1`
    FOREIGN KEY (`escuela_id`)
    REFERENCES `estudiantes`.`escuela` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `estudiantes`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estudiantes`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mail` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `createdAt` DATETIME NOT NULL,
  `updatedAt` DATETIME NULL,
  `hash_publico` VARCHAR(255) NOT NULL,
  `hash_privado` VARCHAR(255) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `estudiante_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_usuario_estudiante1_idx` (`estudiante_id` ASC),
  CONSTRAINT `fk_usuario_estudiante1`
    FOREIGN KEY (`estudiante_id`)
    REFERENCES `estudiantes`.`estudiante` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
