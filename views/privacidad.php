<?=
$this->layout('layouts/app', [
    'titulo' => 'Aviso de privacidad',
    'view' => 'privacidad',
    'access' => 'public',
    'keywords' => 'desarrollo de software, desarrollo, software, tecnologia, micro servicios, latinoamerica, tecnologia mexicana, sistemas, programacion',
    'description' => 'Somos una empresa de Desarrollo de Software Mexicana y estamos comprometidos con la integridad de los datos de todos nuestros clientes'
]);
?>
<main 
    id="privacidad"
    name="Hamilton And Lovelace"
    content="Somos una empresa de Desarrollo de Software Mexicana con el objetivo de crear las mejores herramientas tecnologicas para el mercado Latinoamericano"
></main>
