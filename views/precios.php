<?=
$this->layout('layouts/app', [
    'titulo' => 'Conoce nuestros precios',
    'view' => 'precios',
    'access' => 'public',
    'keywords' => 'desarrollo de software, desarrollo, software, tecnologia, micro servicios, latinoamerica, tecnologia mexicana, sistemas, programacion',
    'description' => 'Somos una empresa de Desarrollo de Software Mexicana, nuestro objetivo es brindarte la mejor relacion costo beneficio del mercado'
]);
?>
<main 
    id="precios"
    name="Hamilton And Lovelace"
    content="Somos una empresa de Desarrollo de Software Mexicana con el objetivo de crear las mejores herramientas tecnologicas para el mercado Latinoamericano"
></main>
