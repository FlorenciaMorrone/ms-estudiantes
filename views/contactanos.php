<?=
$this->layout('layouts/app', [
    'titulo' => 'Contáctanos',
    'view' => 'contactanos',
    'access' => 'public',
    'keywords' => 'desarrollo de software, desarrollo, software, tecnologia, micro servicios, latinoamerica, tecnologia mexicana, sistemas, programacion',
    'description' => 'Somos una empresa de Desarrollo de Software Mexicana con el objetivo de crear las mejores herramientas tecnologicas para el mercado Latinoamericano'
]);
?>
<main 
    id="contactanos"
    name="Hamilton And Lovelace"
    content="Somos una empresa de Desarrollo de Software Mexicana con el objetivo de crear las mejores herramientas tecnologicas para el mercado Latinoamericano"
></main>
