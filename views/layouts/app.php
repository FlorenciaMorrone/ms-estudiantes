<?php 

    $assetsRoute = 'private-assets';

    if($access == 'public')
        $assetsRoute = 'public-assets';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?= $description ?>">
    <meta name="author" content="Hamilton And Lovelace S.A.S" />
    <meta name="copyright" content="Propietario del Hamilton And Lovelace S.A.S todos los derechos reservados" />
    <meta name="keywords" content="<?= $keywords ?>">
    <title><?= (isset($titulo)) ? $titulo : 'Hamilton And Lovelace' ?></title>
    <link rel="shortcut icon" href="public-assets/imagenes?path=logo.png" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Montserrat:wght@100&display=swap" rel="stylesheet">
    <!-- bootstrap styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
    <!-- estilos generales -->
    <link rel="stylesheet" href="<?= $assetsRoute?>/css?path=<?= $view?>">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
    <?= $this->section('content');?>
    <script type='module' src='<?= $assetsRoute?>/js?path=<?= $view?>'></script>
</body>

</html>