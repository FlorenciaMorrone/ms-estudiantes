<?=
$this->layout('layouts/app', [
    'titulo' => 'Hamilton And Lovelace',
    'view' => '404',
    'access' => 'public',
    'keywords' => 'desarrollo de software, desarrollo, software, tecnologia, micro servicios, latinoamerica, tecnologia mexicana, sistemas, programacion',
    'description' => 'Somos una empresa de Desarrollo de Software Mexicana con el objetivo de crear las mejores herramientas tecnologicas para el mercado Latinoamericano'
]);
?>
<main 
    id="404"
    name="Hamilton And Lovelace"
    content="Somos una empresa de Desarrollo de Software Mexicana con el objetivo de crear las mejores herramientas tecnologicas para el mercado Latinoamericano"
></main>
