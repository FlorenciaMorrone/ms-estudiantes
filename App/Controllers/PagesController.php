<?php namespace App\Controllers;

use App\Core\Abstracts\AbstractController;

class PagesController extends AbstractController
{
    public function home()
    {
        return $this->getView('home');
    }

    public function precios()
    {
        return $this->getView('precios');
    }

    public function contactanos()
    {
        return $this->getView('contactanos');
    }

    public function privacidad()
    {
        return $this->getView('privacidad');
    }
}