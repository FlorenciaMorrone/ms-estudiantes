<?php namespace App\Controllers;

use App\Core\Abstracts\AbstractController;

class AssetsController extends AbstractController
{
    public function assets()
    {
        return $this->getView('404');
    }

    public function js()
    {
        $path = './storage/assets/'.$this->getPath().'/app.js';

        if(!@file_exists($path))
            return $this->getView('404');

        return $this->getStorage($path, 'application/javascript');
    }

    public function css()
    {
        $path = './storage/assets/'.$this->getPath().'/app.css';
    
        if(!@file_exists($path))
            return $this->getView('404');

        return $this->getStorage($path, 'text/css');
    }

    public function imagenes()
    {
        $path = './storage/imagenes/'.$this->getPath();

        if(!@file_exists($path))
            return $this->getView('404');

        return $this->getStorage($path);
    }
}