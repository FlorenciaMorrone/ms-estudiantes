<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;

class Escuela extends Repo
{
    public static function crear($escuela)
    {
        Conexion::openConexion();
        $sql = 'INSERT INTO escuela';
        $sql .= '(nombre, grado, grupo) ';
        $sql .= 'VALUES (:nombre_escuela, :grado, :grupo)';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(
            ':nombre_escuela',
            $escuela['nombre_escuela'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(':grado', $escuela['grado'], PDO::PARAM_STR);
        $sentencia->bindValue(':grupo', $escuela['grupo'], PDO::PARAM_STR);
        $sentencia->execute();
        $lastId = $conexion->lastInsertId();
        $sql = 'SELECT * FROM escuela WHERE id = :id';
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_INT);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    public static function todas()
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM escuela';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function traerPorNombre($nombreEscuela)
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM escuela WHERE nombre = :nombre';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':nombre', $nombreEscuela, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }
    public static function traerPorId($id)
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM escuela WHERE id = :id';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $id, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }
}
