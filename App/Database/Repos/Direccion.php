<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;

class Direccion extends Repo
{
    public static function crear($direccion)
    {
        Conexion::openConexion();
        $sql = 'INSERT INTO direccion';
        $sql .=
            '(pais, provincia, ciudad, codigo_postal, calle, numero_principal, numero_secundario) ';
        $sql .=
            'VALUES (:pais, :provincia, :ciudad, :codigo_postal, :calle, :numero_principal, :numero_secundario)';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':pais', $direccion['pais'], PDO::PARAM_STR);
        $sentencia->bindValue(
            ':provincia',
            $direccion['provincia'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(':ciudad', $direccion['ciudad'], PDO::PARAM_STR);
        $sentencia->bindValue(
            ':codigo_postal',
            $direccion['codigo_postal'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(':calle', $direccion['calle'], PDO::PARAM_STR);
        $sentencia->bindValue(
            ':numero_principal',
            $direccion['numero_principal'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(
            ':numero_secundario',
            $direccion['numero_secundario'],
            PDO::PARAM_STR
        );
        $sentencia->execute();
        $lastId = $conexion->lastInsertId();
        $sql = 'SELECT * FROM direccion WHERE id = :id';
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_INT);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    public static function traerPorId($id)
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM direccion WHERE id = :id';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $id, PDO::PARAM_INT);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }
}
