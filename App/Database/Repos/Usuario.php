<?php namespace App\Database\Repos;

use PDO;
use stdClass;
use App\Database\Repo;
use App\Core\Tokenizer;
use App\Database\Conexion;
use App\Database\Repos\Direccion;

class Usuario extends Repo
{
    public static function todos()
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM usuario';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->execute();
        Conexion::closeConexion();
        $res = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $obj = new stdClass();
        $lista = [];
        foreach ($res as $item) {
            $obj->mail = $item['mail'];
            $estudiante = Estudiante::traerPorId($item['estudiante_id']);
            $obj->primer_nombre = $estudiante['primer_nombre'];
            $obj->segundo_nombre = $estudiante['segundo_nombre'];
            $obj->primer_apellido = $estudiante['primer_apellido'];
            $obj->segundo_apellido = $estudiante['segundo_apellido'];
            $escuela = Escuela::traerPorId($estudiante['escuela_id']);
            $obj->escuela = $escuela['nombre'];
            $obj->grado = $escuela['grado'];
            $obj->grupo = $escuela['grupo'];
            array_push($lista, unserialize(serialize($obj)));
        }
        return $lista;
    }

    public static function crear($data)
    {
        if ($direccion = Direccion::crear($data)) {
            if ($escuela = Escuela::crear($data)) {
                if (
                    $estudiante = Estudiante::crear(
                        $data,
                        $direccion['id'],
                        $escuela['id']
                    )
                ) {
                    $tokens = Tokenizer::createFirstToken($data['mail']);
                    Conexion::openConexion();
                    $sql = 'INSERT INTO usuario (mail, 
                                                password, 
                                                createdAt, 
                                                updatedAt, 
                                                estudiante_id, 
                                                hash_publico, 
                                                hash_privado,
                                                isActive)
                                            VALUES(
                                                :mail,
                                                :password,
                                                NOW(), 
                                                NOW(),
                                                :estudiante_id,
                                                :hash_publico, 
                                                :hash_privado,
                                                false)';

                    $conexion = Conexion::getConexion();
                    $sentencia = $conexion->prepare($sql);
                    $sentencia->bindValue(
                        ':hash_publico',
                        $tokens['hash_publico'],
                        PDO::PARAM_STR
                    );
                    $sentencia->bindValue(
                        ':hash_privado',
                        $tokens['hash_privado'],
                        PDO::PARAM_STR
                    );
                    $sentencia->bindValue(
                        ':mail',
                        $data['mail'],
                        PDO::PARAM_STR
                    );
                    $sentencia->bindValue(
                        ':password',
                        hash('sha256', $data['password']),
                        PDO::PARAM_STR
                    );
                    $sentencia->bindValue(
                        ':estudiante_id',
                        $estudiante['id'],
                        PDO::PARAM_STR
                    );
                    $sentencia->execute();
                    $lastId = $conexion->lastInsertId();
                    $sql = 'SELECT * FROM usuario WHERE id = :id';
                    $sentencia = $conexion->prepare($sql);
                    $sentencia->bindValue(':id', $lastId, PDO::PARAM_INT);
                    $sentencia->execute();
                    Conexion::closeConexion();
                    return $sentencia->fetch(PDO::FETCH_ASSOC);
                }
            }
        }
        return null;
    }

    public static function traerPorId($id)
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM usuario WHERE id = :id';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $id, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    public static function traerPorMail($mail)
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM usuario WHERE mail = :mail';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':mail', $mail, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }
}
