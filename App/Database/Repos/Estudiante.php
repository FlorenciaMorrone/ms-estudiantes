<?php namespace App\Database\Repos;

use PDO;
use App\Database\Repo;
use App\Database\Conexion;

class Estudiante extends Repo
{
    public static function crear($estudiante, $direccionId, $escuelaId)
    {
        Conexion::openConexion();
        $sql = 'INSERT INTO estudiante';
        $sql .= '(  primer_nombre, 
                segundo_nombre, 
                primer_apellido, 
                segundo_apellido, 
                sexo, 
                telefono, 
                nombre_contacto, 
                telefono_contacto, 
                direccion_id, 
                escuela_id)';
        $sql .= 'VALUES (
                :primer_nombre, 
                :segundo_nombre, 
                :primer_apellido, 
                :segundo_apellido, 
                :sexo, 
                :telefono, 
                :nombre_contacto, 
                :telefono_contacto, 
                :direccion_id, 
                :escuela_id)';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(
            ':primer_nombre',
            $estudiante['primer_nombre'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(
            ':segundo_nombre',
            $estudiante['segundo_nombre'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(
            ':primer_apellido',
            $estudiante['primer_apellido'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(
            ':segundo_apellido',
            $estudiante['segundo_apellido'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(':sexo', $estudiante['sexo'], PDO::PARAM_STR);
        $sentencia->bindValue(
            ':telefono',
            $estudiante['telefono'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(
            ':nombre_contacto',
            $estudiante['nombre_contacto'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(
            ':telefono_contacto',
            $estudiante['telefono_contacto'],
            PDO::PARAM_STR
        );
        $sentencia->bindValue(':direccion_id', $direccionId, PDO::PARAM_STR);
        $sentencia->bindValue(':escuela_id', $escuelaId, PDO::PARAM_STR);

        $sentencia->execute();
        $lastId = $conexion->lastInsertId();
        $sql = 'SELECT * FROM estudiante WHERE id = :id';
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $lastId, PDO::PARAM_INT);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    public static function traerPorId($id)
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM estudiante WHERE id = :id';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':id', $id, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }

    public static function traerPorMail($mail)
    {
        Conexion::openConexion();
        $sql = 'SELECT * FROM estudiante WHERE mail = :mail';
        $conexion = Conexion::getConexion();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bindValue(':mail', $mail, PDO::PARAM_STR);
        $sentencia->execute();
        Conexion::closeConexion();
        return $sentencia->fetch(PDO::FETCH_ASSOC);
    }
}
