<?php namespace App\Core;

class HttpClient
{
    private $url;

    private $hash;

    private $data;

    private $filters;

    private $files;

    private $method;

    private $endpoint;

    public function __construct($url, $hash)
    {
        $this->url = $url;

        $this->hash = $hash;
    }

    public function addData($data)
    {
        $this->data = json_encode($data, true);
    }

    public function addFilters($filters)
    {
        $this->filters = json_encode($filters, true);
    }

    public function addFiles($files)
    {
        $this->files = $files;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function addEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function execute()
    {

    }

}