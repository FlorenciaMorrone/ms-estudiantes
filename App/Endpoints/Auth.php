<?php namespace App\Endpoints;

use App\Database\Repos\Usuario;
use App\Core\Abstracts\AbstractEndpoints;
use App\Database\Repos\Sesiones;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;

class Auth extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;

    public function login()
    {
        if (
            !($usuario = Usuario::validateUniquerule(
                'usuario',
                'mail',
                $this->getParam('mail')
            ))
        ) {
            return self::makeEndpointError(
                'NE',
                [
                    'name' => 'NoEncontrado',
                    'description' => 'Usuario no encontrado',
                ],
                400
            );
        }

        if (
            $usuario['password'] != hash('sha256', $this->getParam('password'))
        ) {
            return self::makeEndpointError(
                'CI',
                [
                    'name' => 'CredencialesIncorrectas',
                    'description' => 'Error en las credenciales ingresadas',
                ],
                400
            );
        }

        if (!($sesion = Sesiones::traerSesionPorIdUsuario($usuario['id']))) {
            $sesionNueva = Sesiones::crearSesion($usuario);
        }

        if (isset($sesion['isActive']) && $sesion['isActive']) {
            return new JsonResponse(
                [
                    '_error' => [
                        'message' => 'El usuario esta actualmente activo',
                    ],
                ],
                400
            );
        }

        $token = isset($sesionNueva)
            ? $sesionNueva
            : Sesiones::activarSesion($sesion);

        $response = new JsonResponse([
            '_data' => [
                'message' => 'Welcome!',
            ],
        ]);

        $response->headers->setCookie(new Cookie('_auth', $token));

        return $response;
    }

    public function logout()
    {
        Sesiones::cerrarSesion($this->getUserId());

        $response = new JsonResponse([
            '_data' => [
                'message' => 'See you!',
            ],
        ]);

        $response->headers->setCookie(new Cookie('_auth', ''));

        return $response;
    }

    public function request()
    {
        if (
            !($usuario = Usuario::validateUniquerule(
                'usuario',
                'mail',
                $this->getParam('mail')
            ))
        ) {
            return self::makeEndpointError(
                'NotFound',
                [
                    'name' => 'NotFound',
                    'description' => 'Mail Not Found',
                ],
                400
            );
        }
        dd($usuario);

        return new JsonResponse(
            [
                '_data' => [
                    'message' =>
                        'Se te ha enviado un codigo de validacion a tu correo electronico',
                ],
            ] . 200
        );
    }

    public function confirm()
    {
    }
}
