<?php namespace App\Endpoints;

use App\Database\Repos\Usuario;
use App\Core\Abstracts\AbstractEndpoints;
use App\Database\Repos\Clientes as ReposClientes;
use App\Database\Repos\Contactos;
use App\Database\Repos\Escuela;
use App\Database\Repos\Estudiante;
use App\Database\Repos\Sesiones;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;

class Escuelas extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;

    public function todas()
    {
        $escuelas = Escuela::todas();

        return new JsonResponse(
            [
                '_data' => [
                    'total escuelas' => count($escuelas),
                    'escuela' => $escuelas,
                ],
            ],
            200
        );
    }

    public function crearEscuela()
    {
        if (Escuela::traerPorNombre($this->getParam['nombre'])) {
            return new JsonResponse(
                [
                    '_errors' => [
                        'mensaje' => 'Escuela ya registrada',
                    ],
                ],
                200
            );
        }

        if ($escuela = Escuela::crear($this->getData())) {
            return new JsonResponse(
                [
                    '_data' => [
                        'mensaje' => 'Escuela registrada con exito',
                    ],
                ],
                200
            );
        }
    }
}
