<?php namespace App\Endpoints;

use App\Database\Repos\Usuario;
use App\Core\Abstracts\AbstractEndpoints;
use App\Database\Repos\Direccion as ReposDireccion;
use Symfony\Component\HttpFoundation\JsonResponse;

class Direcciones extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;

    public function crearDireccion()
    {
        if ($direccion = ReposDireccion::crear($this->getData())) {
            return new JsonResponse(
                [
                    '_data' => [
                        'mensaje' => 'Direccion agregada con exito',
                    ],
                ],
                200
            );
        } else {
            return new JsonResponse(
                [
                    '_data' => [
                        'mensaje' => 'Error al crear Direccion',
                    ],
                ],
                200
            );
        }
    }
}
