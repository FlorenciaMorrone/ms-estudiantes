<?php namespace App\Endpoints;

use App\Database\Repos\Usuario;
use App\Core\Abstracts\AbstractEndpoints;
use App\Database\Repos\Direccion;
use App\Database\Repos\Escuela;
use App\Database\Repos\Estudiante;
use App\Database\Repos\Sesiones;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;

class Estudiantes extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;

    public function todos()
    {
        $estudiante = Usuario::todos();

        return new JsonResponse(
            [
                '_data' => [
                    'totalestudiante' => count($estudiante),
                    'estudiante' => $estudiante,
                ],
            ],
            200
        );
    }

    public function crearEstudiante()
    {
        if (Estudiante::traerPorMail($this->getParam('mail'))) {
            return new JsonResponse(
                [
                    '_errors' => [
                        'mensaje' => 'Estudiante ya registrado',
                    ],
                ],
                200
            );
        }
    }
}
