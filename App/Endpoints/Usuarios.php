<?php namespace App\Endpoints;

use App\Database\Repos\Usuario;
use App\Core\Abstracts\AbstractEndpoints;
use App\Database\Repos\Clientes as ReposClientes;
use App\Database\Repos\Sesiones;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;

class Usuarios extends AbstractEndpoints
{
    use \App\Core\Validators\MakeErrorTrait;

    public function todos()
    {
        $usuarios = Usuario::todos();
        return new JsonResponse(
            [
                '_data' => [
                    'totalUsuarios' => count($usuarios),
                    'usuario' => $usuarios,
                ],
            ],
            200
        );
    }

    public function crearUsuario()
    {
        $data = $this->getData();
        if ($usuario = Usuario::traerPorMail($data['mail'])) {
            return new JsonResponse(
                [
                    '_error' => [
                        'mensaje' => 'Usuario ya existe',
                    ],
                ],
                200
            );
        }

        if ($usuario = Usuario::crear($data)) {
            return new JsonResponse(
                [
                    '_data' => [
                        'mensaje' => 'Usuario creado con éxito',
                        'mail' => $usuario['mail'],
                    ],
                ],
                200
            );
        } else {
            return new JsonResponse(
                [
                    '_error' => [
                        'mensaje' => 'Error al crear usuario',
                    ],
                ],
                200
            );
        }
    }
}
